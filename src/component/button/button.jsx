import React from 'react';

import './button.style.scss';

const Button = (props) => {
    return(
        <button 
            id={props.id}
            className={props.className}
            onClick={props.handleClick}>
                {props.text}
        </button>
    )
}

export default Button;
