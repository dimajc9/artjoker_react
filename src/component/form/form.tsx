import { useState, SetStateAction, Dispatch } from 'react';

import Button from '../button/button';

import Posts from '../../posts/posts';
import { post } from '../../posts/posts';

import './form.style.scss'

const Form = (): JSX.Element => {
    const [post, setPost]: [post[], Dispatch<SetStateAction<post[]>>]= useState(Posts());
    const [evenPost, setEvenPost]: [boolean, Dispatch<SetStateAction<boolean>>] = useState(Boolean);
    const [windowText, setWindowText]: [boolean, Dispatch<SetStateAction<boolean>>] = useState(Boolean);
    const [postText, setPostText] = useState('');
    const [postHeading, setPostHeading] = useState('');

    const renderButton = () => {
        return post.map((item, i) => {
            return (
                <Button className="button" handleClick={() => {
                        setWindowText(true);
                        setEvenPost(true);
                        setPostText(item.post);
                        setPostHeading(item.heading);
                    }
                } text={item.heading} key={i}/>
            )
        })
    }

    const addPost = () => {
        return(
            <>
                <p>{postHeading}</p>
                <p>{postText}</p>
            </>
        )
    }

    return (
        <>
            <Button className="button" text='delete' handleClick={() => setWindowText(false)}/>
            <div className="postButton"> 
                { renderButton() }
                { windowText ? <div className = 'postButton_text'>
                    { evenPost ? addPost() : ''}
                </div> : ''}
            </div> 
        </>
    );
}

export default Form;