export interface post{
    heading: string,
    post: string
}

const Posts = (): post[] => {
    const arg: post[] = [{
        heading: 'films', 
        post: 'luptatibus eligendi quod earum minima inventore! Vitae nostrum, itaque omnis quae quia quod, quasi eveniet voluptates soluta quibusdam iste officiis ut tempore sint ex. Doloremque veritatis, sed laborum iste voluptatum aut nulla fugiat dicta sit. Facere fuga repellat, cupiditate molestiae atque dolore temporibus amet necessitatibus dolores nemo ullam placeat sed accusamus architecto. Hic?',
    },{
        heading: 'music', 
        post: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Vero consequatur adipisci iure sint rerum perspiciatis crrupti necessitatibus consectetur dolorem, repellendus impedit quidem, laboriosam beatae officia corporis quia eligendi voluptatibus optio perferendis reprehenderit eveniet illum? Facere id, error nostrum dolore provident suscipit aliquid odio natus aspernatur fugit. Maiores voluptatibus eligendi quod earum minima inventore! Vitae nostrum, itaque omnis quae quia quod, quasi eveniet voluptates soluta quibusdam iste officiis ut tempore sint ex. Doloremque veritatis, sed laborum iste voluptatum aut nulla fugiat dicta sit. Facere fuga repellat, cupiditate molestiae atque dolore temporibus amet necessitatibus dolores nemo ullam placeat sed accusamus architecto. Hic?',
    },{
        heading: 'serials', 
        post: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Vero consequatur adipisci iure sint rerum perspiciatis corrupti necessitatibus consectetur dolorem, repellendus impedit quidem, laboriosam beatae officia corporis quia eligendi voluptatibus optio perferendis reprehenderit eveniet illum? Facere id, error nostrum dolore provident suscipit aliquid odio natus aspernatur fugit. Maiores voluptatibus eligendi quod earum minima inventore! Vitae nostrum, itaque omnis quae quia quod, quasi eveniet voluptates soluta quibusdam iste officiis ut tempore sint ex. Doloremque veritatis, dolores nemo ullam placeat sed accusamus architecto. Hic?',
    },{
        heading: 'video', 
        post: 'Lorem ipsum, dolor sit piciatis corrupti necessitatibus consectetur dolorem, repellendus impedit quidem, laboriosam beatae officia corporis quia eligendi voluptatibus optio perferendis reprehenderit eveniet illum? Facere id, error nostrum dolore provident suscipit aliquid odio natus aspernatur fugit. Maiores voluptatibus eligendi quod earum minima inventore! Vitae nostrum, itaque omnis quae quia quod, quasi eveniet voluptates soluta quibusdam iste officiis ut tempore sint ex. Doloremque veritatis, sed laborum iste voluptatum aut nulla fugiat dicta sit. Facere fuga repellat, cupiditate molestiae atque dolore temporibus amet necessitatibus dolores nemo ullam placeat sed accusamus architecto. Hic?',
    },{
        heading: 'something', 
        post: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Vero consequatur adipisci iure sint rerumdolore provident suscipit aliquid odio natus aspernatur fugit. Maiores voluptatibus eligendi quod earum minima inventore! Vitae nostrum, itaque omnis quae quia quod, quasi eveniet voluptates soluta quibusdam iste officiis ut tempore sint ex. Doloremque veritatis, sed laborum iste voluptatum aut nulla fugiat dicta sit. Facere fuga repellat, cupiditate molestiae atque dolore temporibus amet necessitatibus dolores nemo ullam placeat sed accusamus architecto. Hic?',
    }]

    return arg;
}

export default Posts;
